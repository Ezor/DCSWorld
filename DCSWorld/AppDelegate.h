//
//  AppDelegate.h
//  DCSWorld
//
//  Created by Julien Rozé on 03/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
