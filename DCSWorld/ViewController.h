//
//  ViewController.h
//  DCSWorld
//
//  Created by Julien Rozé on 03/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCDAsyncUdpSocket.h"
#import "ImageScrollView.h"

@interface ViewController : UIViewController

-(void)sendCommand:(NSString*)command;
-(void)bind;

@end
