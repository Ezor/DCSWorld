//
//  Instrument.m
//  DCSWorld
//
//  Created by Julien Rozé on 04/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import "Instrument.h"
#import "ViewController.h"

@implementation Instrument

@synthesize mainController = _mainController;

-(id)initWithMainController:(ViewController*)mainController {
    self = [super init];
    if (self) {
        self.mainController = mainController;
        self.frame = [self getFrame];
        [self layout];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)updateWithData:(NSDictionary*)data {
    
}

-(CGRect)getFrame {
    return CGRectZero;
}

-(void)layout {
    
}

@end
