/*
     File: ImageScrollView.m
 Abstract: Centers image within the scroll view and configures image sizing and display.
  Version: 1.0
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
 */

#import "ImageScrollView.h"
#import "TilingView.h"
#import "PlanView.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DrawView.h"

@implementation ImageScrollView
@synthesize index;

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.bouncesZoom = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;
        self.backgroundColor = [UIColor clearColor];
        self.alwaysBounceHorizontal = NO;
        self.alwaysBounceVertical = NO;
        self.bouncesZoom = NO;
        self.bounces = NO;
        self.backgroundColor = [UIColor blackColor];
        
        planeView = [[PlanView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        autoFollow = YES;
        mini = YES;

    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.bouncesZoom = YES;
        self.decelerationRate = UIScrollViewDecelerationRateFast;
        self.delegate = self;  
        self.backgroundColor = [UIColor clearColor];
        self.alwaysBounceHorizontal = NO;
        self.alwaysBounceVertical = NO;
        self.bouncesZoom = NO;
        self.bounces = NO;
        self.backgroundColor = [UIColor blackColor];
        
        planeView = [[PlanView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        autoFollow = YES;
        mini = YES;
        
    }
    return self;
}

#pragma mark -
#pragma mark Override layoutSubviews to center content

- (void)layoutSubviews 
{
    [super layoutSubviews];
    
    // center the image as it becomes smaller than the size of the screen

    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = imageView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    imageView.frame = frameToCenter;
    
    /*if ([imageView isKindOfClass:[TilingView class]]) {
        // to handle the interaction between CATiledLayer and high resolution screens, we need to manually set the
        // tiling view's contentScaleFactor to 1.0. (If we omitted this, it would be 2.0 on high resolution screens,
        // which would cause the CATiledLayer to ask us for tiles of the wrong scales.)
        imageView.contentScaleFactor = 1.0;
    }*/
}

#pragma mark -
#pragma mark UIScrollView delegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageView;
}

-(void)scrollViewDidZoom:(UIScrollView *)scrollView {
    NSLog(@"%f", scrollView.zoomScale);
    [self setPlaneLatitude:lastLat longitude:lastLong heading:lastHeading];
    //NSLog(@"%@", NSStringFromCGPoint(self.contentOffset));
}

#pragma mark -
#pragma mark Configure scrollView to display new image (tiled or not)

- (void)displayTiledImageNamed:(NSString *)imageName size:(CGSize)imageSize
{
    originalFrame = self.superview.frame;
	originalSize = imageSize;
    miniSuperView = self.superview.superview;
	
    // clear the previous imageView
    [imageView removeFromSuperview];
    imageView = nil;
    
    // reset our zoomScale to 1.0 before doing any further calculations
    self.zoomScale = 1.0;
    
    // make a new TilingView for the new image
    imageView = [[TilingView alloc] initWithImageName:imageName size:imageSize];
    [(TilingView *)imageView setAnnotates:NO]; // ** remove this line to remove the white tile grid **
	
	UITapGestureRecognizer *singleFingerDTap = [[UITapGestureRecognizer alloc]
												initWithTarget:self action:@selector(handleTapFrom:)];
	singleFingerDTap.numberOfTapsRequired = 2;
	singleFingerDTap.delegate = self;
	[imageView addGestureRecognizer:singleFingerDTap];
    
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc]
												initWithTarget:self action:@selector(handleLongGesture:)];
	longGesture.numberOfTouchesRequired = 2;
    longGesture.minimumPressDuration = 0.5;
	longGesture.delegate = self;
	[imageView addGestureRecognizer:longGesture];
	
    [self addSubview:imageView];
    
    [imageView addSubview:planeView];
    
    [self configureForImageSize:imageSize];
    
    draw = [[DrawView alloc] initWithFrame:imageView.bounds];
    draw.backgroundColor = [UIColor clearColor];
    draw.userInteractionEnabled = NO;
    [imageView addSubview:draw];
    
    
}

-(void)handleLongGesture:(UILongPressGestureRecognizer*)gesture {
    
    if(gesture.state == UIGestureRecognizerStateBegan) {
        CGPoint point1 = [gesture locationOfTouch:0 inView:_distanceView];
        CGPoint point2 = [gesture locationOfTouch:1 inView:_distanceView];
        
        CGPoint pointCoord1 = [gesture locationOfTouch:0 inView:imageView];
        CGPoint pointCoord2 = [gesture locationOfTouch:1 inView:imageView];
        
        double mapLatMin = 45.5;
        double mapLatMax = 41.0;
        double mapLongMin = 37.0;
        double mapLongMax = 46.0;
        
        double pointLat1 = (mapLatMax-mapLatMin)*pointCoord1.y/originalSize.height;
        double pointLat2 = (mapLatMax-mapLatMin)*pointCoord2.y/originalSize.height;
        
        double pointLong1 = (mapLongMax-mapLongMin)*pointCoord1.x/originalSize.width;
        double pointLong2 = (mapLongMax-mapLongMin)*pointCoord2.x/originalSize.width;
            
        [_distanceView displayDistanceBetweenPoint:point1 andPoint:point2 lat1:pointLat1 long1:pointLong1 lat2:pointLat2 long2:pointLong2];
    }
    
    if(gesture.state == UIGestureRecognizerStateEnded) {
        _distanceView.hidden = YES;
    }
    
}

- (void)handleTapFrom:(UITapGestureRecognizer *)recognizer {
	
	//[UIView beginAnimations:nil context:NULL];
    //[UIView setAnimationDuration:0.3];
    
	CGPoint point = [recognizer locationInView:imageView];
			
	if(self.zoomScale > self.minimumZoomScale) {
		//self.zoomScale = self.minimumZoomScale;
		//self.contentOffset = CGPointMake(0, 0);
		[self setZoomScale:self.minimumZoomScale animated:NO];
		[self setContentOffset:CGPointMake(0, 0) animated:NO];
	} else {
		//self.zoomScale = self.maximumZoomScale;
		//self.contentOffset = CGPointMake(x, y);
		[self setZoomScale:self.maximumZoomScale animated:NO];
		double mapWidth = originalSize.width * self.zoomScale;
        double mapHeight = originalSize.height * self.zoomScale;
        
        double xInPlan = point.x * mapWidth / originalSize.width;
        double yInPlan = point.y * mapHeight / originalSize.height;
        
        [self scrollRectToVisible:CGRectMake(xInPlan-(self.frame.size.width/2), yInPlan-(self.frame.size.height/2), self.frame.size.width, self.frame.size.height) animated:YES];
	}
	
	
	
	/*CGPoint point = [recognizer locationInView:self];
	CGFloat mainWidth = self.frame.size.width;
	CGFloat mainHeight = self.frame.size.height;
	CGFloat x = ((point.x - recognizer.view.frame.origin.x) * self.minimumZoomScale) - (mainWidth / 2);
	CGFloat y = ((point.y - recognizer.view.frame.origin.y) * self.minimumZoomScale) - (mainHeight / 2);
	
	NSLog(@"%f, %f", point.x, point.y);
	
	self.contentOffset = CGPointMake(point.x*self.minimumZoomScale, point.y*self.minimumZoomScale);*/
	
    //[UIView commitAnimations];
	
}

- (void)configureForImageSize:(CGSize)imageSize
{
    CGSize boundsSize = [self bounds].size;
                
    // set up our content size and min/max zoomscale
    CGFloat xScale = boundsSize.width / imageSize.width;    // the scale needed to perfectly fit the image width-wise
    CGFloat yScale = boundsSize.height / imageSize.height;  // the scale needed to perfectly fit the image height-wise
    CGFloat minScale = MIN(xScale, yScale);                 // use minimum of these to allow the image to become fully visible
    
    // on high resolution screens we have double the pixel density, so we will be seeing every pixel if we limit the
    // maximum zoom scale to 0.5.
    CGFloat maxScale = 1.0;

    // don't let minScale exceed maxScale. (If the image is smaller than the screen, we don't want to force it to be zoomed.) 
    if (minScale > maxScale) {
        minScale = maxScale;
    }
        
    self.contentSize = imageSize;
    self.maximumZoomScale = maxScale;
    self.minimumZoomScale = 0.146168;
    self.zoomScale = maxScale;  // start out with the content fully visible
}

-(void)setPlaneLatitude:(double)latitude longitude:(double)longitude heading:(double)heading {
    
    // Lat = 41
    // Long = 44
    //double currentWidth = originalSize.width / self.zoomScale;
    double mapLatMin = 45.5;
    double mapLatMax = 41.0;
    double mapLongMin = 37.0;
    double mapLongMax = 46.0;
    
    
    double lat = latitude - mapLatMax;
    double longi = longitude - mapLongMin;
    
    double y = originalSize.height * lat / (mapLatMin-mapLatMax);
    y = originalSize.height - y;
    double x = originalSize.width * longi / (mapLongMax-mapLongMin);
    
    double planeSize = 40 / self.zoomScale;
    BOOL redraw = planeSize != planeView.frame.size.width;
    planeView.transform = CGAffineTransformIdentity;
    planeView.frame = CGRectMake(x-(planeSize/2), y-(planeSize/2), planeSize, planeSize);
    if(redraw) {
        
        [planeView setNeedsDisplay];
    }
    planeView.transform = CGAffineTransformMakeRotation(heading);    
    
    lastHeading = heading;
    lastLat = latitude;
    lastLong = longitude;
    
    if(autoFollow) {
        double mapWidth = originalSize.width * self.zoomScale;
        double mapHeight = originalSize.height * self.zoomScale;
        
        double xInPlan = x * mapWidth / originalSize.width;
        double yInPlan = y * mapHeight / originalSize.height;
        
        
        [self scrollRectToVisible:CGRectMake(xInPlan-(self.frame.size.width/2), yInPlan-(self.frame.size.height/2), self.frame.size.width, self.frame.size.height) animated:YES];
    }
}

-(IBAction)segmentedControlChanged:(UISegmentedControl*)sender {
    if(sender.selectedSegmentIndex == 0) {
        autoFollow = NO;
        self.pinchGestureRecognizer.enabled = YES;
        self.panGestureRecognizer.enabled = YES;
        draw.userInteractionEnabled = NO;
    } else if(sender.selectedSegmentIndex == 1) {
        autoFollow = YES;
        self.pinchGestureRecognizer.enabled = YES;
        self.panGestureRecognizer.enabled = YES;
        draw.userInteractionEnabled = NO;
    } else {
        autoFollow = NO;
        self.pinchGestureRecognizer.enabled = NO;
        self.panGestureRecognizer.enabled = NO;
        draw.userInteractionEnabled = YES;
    }
}

-(IBAction)full:(id)sender {
    if(mini) {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        UIView *mainView = app.viewController.view;
        [mainView addSubview:self.superview];
        self.superview.frame = mainView.bounds;
    } else {
        [miniSuperView addSubview:self.superview];
        self.superview.frame = originalFrame;
    }
    mini = !mini;
}

@end
