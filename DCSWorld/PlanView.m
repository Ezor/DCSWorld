//
//  PlanView.m
//  DCSWorld
//
//  Created by Julien Rozé on 06/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import "PlanView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PlanView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = NO;
        self.layer.anchorPoint = CGPointMake(0.5, 0.5);
        self.alpha = 0.9;
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    [[UIColor redColor] setFill];
    [[UIColor whiteColor] setStroke];
    
    CGFloat margin = rect.size.width / 4.0f;
    CGFloat pointe = rect.size.width / 40.0f;
    
    CGContextSetLineWidth(ctx, rect.size.width / 20.0f);
    
    // Rect is 40x40
    CGContextBeginPath(ctx);
    CGContextMoveToPoint(ctx, rect.size.width/2.0f, margin);
    CGContextAddLineToPoint(ctx, rect.size.width-margin, rect.size.height-margin);
    CGContextAddLineToPoint(ctx, margin, rect.size.height-margin);
    CGContextClosePath(ctx);
    
    CGContextDrawPath(ctx, kCGPathFillStroke);
    
    CGContextBeginPath(ctx);
    CGContextMoveToPoint(ctx, (rect.size.width/2.0f)-pointe, 0);
    CGContextAddLineToPoint(ctx, (rect.size.width/2.0f)+pointe, 0);
    CGContextAddLineToPoint(ctx, (rect.size.width/2.0f)+pointe, margin+pointe);
    CGContextAddLineToPoint(ctx, (rect.size.width/2.0f)-pointe, margin+pointe);
    CGContextClosePath(ctx);
    
    CGContextDrawPath(ctx, kCGPathFillStroke);
}


@end
