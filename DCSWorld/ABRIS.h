//
//  ABRIS.h
//  DCSWorld
//
//  Created by Julien Rozé on 04/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import "Instrument.h"
#import "ImageScrollView.h"

@interface ABRIS : Instrument {
    
}

@property(nonatomic, strong) IBOutlet ImageScrollView *map;
@property(nonatomic, strong) IBOutlet UIView* view;
@property(nonatomic, strong) IBOutlet UIWebView* webView;

@end
