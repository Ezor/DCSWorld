//
//  Datalink.m
//  DCSWorld
//
//  Created by Julien Rozé on 04/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import "Datalink.h"
#import "ViewController.h"

@implementation Datalink

-(void)updateWithData:(NSDictionary *)data {
    if(!data) return;
    self.light_target1.hidden = [[data valueForKey:@"datalink_target1"] doubleValue] == 0.0;
    self.light_target2.hidden = [[data valueForKey:@"datalink_target2"] doubleValue] == 0.0;
    self.light_target3.hidden = [[data valueForKey:@"datalink_target3"] doubleValue] == 0.0;
    self.light_target4.hidden = [[data valueForKey:@"datalink_target4"] doubleValue] == 0.0;
    self.light_wingman1.hidden = [[data valueForKey:@"datalink_wingman1"] doubleValue] == 0.0;
    self.light_wingman2.hidden = [[data valueForKey:@"datalink_wingman2"] doubleValue] == 0.0;
    self.light_wingman3.hidden = [[data valueForKey:@"datalink_wingman3"] doubleValue] == 0.0;
    self.light_wingman4.hidden = [[data valueForKey:@"datalink_wingman4"] doubleValue] == 0.0;
    self.light_all.hidden = [[data valueForKey:@"datalink_all"] doubleValue] == 0.0;
    self.light_sendmem.hidden = [[data valueForKey:@"datalink_send"] doubleValue] == 0.0;
    self.light_clear.hidden = [[data valueForKey:@"datalink_erase"] doubleValue] == 0.0;
    self.light_dlingress.hidden = [[data valueForKey:@"datalink_ingress"] doubleValue] == 0.0;
}

-(CGRect)getFrame {
    return CGRectMake(10, 10, 207.0*2.0, 113.0*2.0);
}

-(void)layout {
    [[NSBundle mainBundle] loadNibNamed:@"Datalink" owner:self options:nil];
    [self addSubview:self.view];
}

-(IBAction)wingman1:(id)sender {
    [self.mainController sendCommand:@"C,25,447,0"];
    [self.mainController sendCommand:@"C,25,784,0"];
}

-(IBAction)wingman2:(id)sender {
    [self.mainController sendCommand:@"C,25,448,0"];
    [self.mainController sendCommand:@"C,25,785,0"];
}

-(IBAction)wingman3:(id)sender {
    [self.mainController sendCommand:@"C,25,449,0"];
    [self.mainController sendCommand:@"C,25,786,0"];
}

-(IBAction)wingman4:(id)sender {
    [self.mainController sendCommand:@"C,25,450,0"];
    [self.mainController sendCommand:@"C,25,787,0"];
}

-(IBAction)all:(id)sender {
    [self.mainController sendCommand:@"C,25,451,0"];
    [self.mainController sendCommand:@"C,25,788,0"];
}

-(IBAction)target1:(id)sender {
    [self.mainController sendCommand:@"C,25,443,0"];
    [self.mainController sendCommand:@"C,25,780,0"];
}

-(IBAction)target2:(id)sender {
    [self.mainController sendCommand:@"C,25,444,0"];
    [self.mainController sendCommand:@"C,25,781,0"];
}

-(IBAction)target3:(id)sender {
    [self.mainController sendCommand:@"C,25,445,0"];
    [self.mainController sendCommand:@"C,25,782,0"];
}

-(IBAction)target4:(id)sender {
    [self.mainController sendCommand:@"C,25,446,0"];
    [self.mainController sendCommand:@"C,25,783,0"];
}

-(IBAction)erase:(id)sender {
    [self.mainController sendCommand:@"C,25,452,0"];
    [self.mainController sendCommand:@"C,25,789,0"];
}

-(IBAction)ingress:(id)sender {
    [self.mainController sendCommand:@"C,25,453,0"];
    [self.mainController sendCommand:@"C,25,790,0"];
}

-(IBAction)send:(id)sender {
    [self.mainController sendCommand:@"C,25,454,0"];
    [self.mainController sendCommand:@"C,25,791,0"];
}





@end
