//
//  DistanceView.h
//  DCSWorld
//
//  Created by Julien Rozé on 07/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistanceView : UIView {
    CGPoint currentPoint1;
    CGPoint currentPoint2;
    double currentZoomScale;
}

-(void)displayDistanceBetweenPoint:(CGPoint)point1 andPoint:(CGPoint)point2 lat1:(double)lat1 long1:(double)long1 lat2:(double)lat2 long2:(double)long2;

@end
