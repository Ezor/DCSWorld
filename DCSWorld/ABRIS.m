//
//  ABRIS.m
//  DCSWorld
//
//  Created by Julien Rozé on 04/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import "ABRIS.h"
#import "ViewController.h"

@implementation ABRIS

-(void)updateWithData:(NSDictionary *)data {
    if(!data) return;
    [_map setPlaneLatitude:[[data valueForKey:@"latitude"] doubleValue] longitude:[[data valueForKey:@"longitude"] doubleValue] heading:[[data valueForKey:@"heading"] doubleValue]];
}

-(CGRect)getFrame {
    return CGRectMake(self.mainController.view.bounds.size.width-484-10, 10, 484, 676);
}

-(void)layout {
    [[NSBundle mainBundle] loadNibNamed:@"ABRIS" owner:self options:nil];
    [self addSubview:self.view];
    [_map displayTiledImageNamed:@"newtheater" size:CGSizeMake(9210, 4605)];
}

-(IBAction)button1:(id)sender {
    [self.mainController sendCommand:@"C,9,496,0"];
    [self.mainController sendCommand:@"C,9,805,0"];
}

-(IBAction)button2:(id)sender {
    [self.mainController sendCommand:@"C,9,497,0"];
    [self.mainController sendCommand:@"C,9,806,0"];
}

-(IBAction)button3:(id)sender {
    [self.mainController sendCommand:@"C,9,498,0"];
    [self.mainController sendCommand:@"C,9,807,0"];
}

-(IBAction)button4:(id)sender {
    [self.mainController sendCommand:@"C,9,499,0"];
    [self.mainController sendCommand:@"C,9,808,0"];
}

-(IBAction)button5:(id)sender {
    [self.mainController sendCommand:@"C,9,500,0"];
    [self.mainController sendCommand:@"C,9,809,0"];
}


@end
