//
//  DistanceView.m
//  DCSWorld
//
//  Created by Julien Rozé on 07/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import "DistanceView.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>

@implementation DistanceView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(NSString*)formattedDistance:(double)value withUnit:(BOOL)displayUnit {
    
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setUsesGroupingSeparator:YES];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumIntegerDigits:1];
    [formatter setGroupingSize:3];
    
        if(value / 1000 > 1) {
            if(displayUnit) {
                return [NSString stringWithFormat:@"%@ km", [formatter stringFromNumber:[NSNumber numberWithDouble:value/1000]]];
            } else {
                return [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithDouble:value/1000]]];
            }
        } else {
            if(displayUnit) {
                return [NSString stringWithFormat:@"%@ m", [formatter stringFromNumber:[NSNumber numberWithDouble:value]]];
            } else {
                return [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithDouble:value]]];
            }
        }
}

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiandsToDegrees(x) (x * 180.0 / M_PI)

- (float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc
{
    float fLat = degreesToRadians(fromLoc.latitude);
    float fLng = degreesToRadians(fromLoc.longitude);
    float tLat = degreesToRadians(toLoc.latitude);
    float tLng = degreesToRadians(toLoc.longitude);
    
    float degree = radiandsToDegrees(atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng)));
    
    if (degree >= 0) {
        return degree;
    } else {
        return 360+degree;
    }
}

-(void)displayDistanceBetweenPoint:(CGPoint)point1 andPoint:(CGPoint)point2 lat1:(double)lat1 long1:(double)long1 lat2:(double)lat2 long2:(double)long2 {
    
    //NSLog(@"1: %@ , 2: %@", NSStringFromCGPoint(point1), NSStringFromCGPoint(point2));
    currentPoint1 = point1;
    currentPoint2 = point2;
    
    CLLocation *from = [[CLLocation alloc] initWithLatitude:lat1 longitude:long1];
    CLLocation *to = [[CLLocation alloc] initWithLatitude:lat2 longitude:long2];
    
    double distance = [to distanceFromLocation:from];
    NSString *sDistance = [self formattedDistance:distance withUnit:YES];
    
    while(self.subviews.count > 0) {
        [[self.subviews objectAtIndex:0] removeFromSuperview];
    }
    
    UILabel *labelDistance = [[UILabel alloc] initWithFrame:CGRectZero];
    labelDistance.backgroundColor = [UIColor clearColor];
    labelDistance.font = [UIFont boldSystemFontOfSize:16];
    labelDistance.textColor = [UIColor blackColor];
    labelDistance.text = sDistance;
    labelDistance.textAlignment = UITextAlignmentCenter;
    [labelDistance sizeToFit];
    
    float bearing = [self getHeadingForDirectionFromCoordinate:CLLocationCoordinate2DMake(lat1, long1) toCoordinate:CLLocationCoordinate2DMake(lat2, long2)];
    bearing = roundf(bearing);
    NSString *sBearing = [NSString stringWithFormat:@"%0.0f° / %0.0f°", bearing, (bearing+180 > 360 ? bearing-180 : bearing+180)];
    
    UILabel *labelBearing = [[UILabel alloc] initWithFrame:CGRectZero];
    labelBearing.backgroundColor = [UIColor clearColor];
    labelBearing.font = [UIFont boldSystemFontOfSize:16];
    labelBearing.textColor = [UIColor redColor];
    labelBearing.text = sBearing;
    labelBearing.textAlignment = UITextAlignmentCenter;
    [labelBearing sizeToFit];
    
    
    CGRect frame = labelBearing.frame.size.width > labelDistance.frame.size.width ? labelBearing.frame : labelDistance.frame;
    frame.size.width += 10;
    frame.size.height = 40;
    
    frame.origin.x = MIN(point1.x, point2.x) + (ABS(point1.x-point2.x) / 2) - (frame.size.width/2);
    frame.origin.y = MIN(point1.y, point2.y) - frame.size.height - 20;
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.borderWidth = 1.0f;
    view.layer.borderColor = [[UIColor blackColor] CGColor];
    view.layer.cornerRadius = 3.0f;
    labelDistance.frame = CGRectMake(0, 0, frame.size.width, 20);
    labelBearing.frame = CGRectMake(0, 20, frame.size.width, 20);
    [view addSubview:labelDistance];
    [view addSubview:labelBearing];
    [self addSubview:view];
    
//    self.frame = CGRectMake(minX, minY, maxX-minX, maxY-minY);
    self.hidden = NO;
    [self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor whiteColor] set];
    
    CGContextSetLineWidth(ctx, 4.0f);
    CGFloat phases[] = {8.0f, 4.0f};
    CGContextSetLineDash(ctx, 1.0f, phases, 2);
    CGContextBeginPath(ctx);
    CGContextMoveToPoint(ctx, currentPoint1.x, currentPoint1.y);
    CGContextAddLineToPoint(ctx, currentPoint2.x, currentPoint2.y);
    CGContextDrawPath(ctx, kCGPathStroke);
    
}

@end
