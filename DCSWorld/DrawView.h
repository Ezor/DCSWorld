//
//  DrawView.h
//  DrawIt
//
//  Created by jroze on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DrawViewDelegate;

@interface DrawView : UIView <UIScrollViewDelegate> {
    CGMutablePathRef _trackingPath;
    CGRect _trackingDirty;
    NSMutableArray *_pochoirs;
    NSMutableArray *_pochoirsNames;
    NSMutableArray *_motifs;
    NSMutableArray *_positionMotifs;
    NSMutableDictionary *_lastModifs;
    CGRect _pochoirRect;
    CGPoint _lastPoint;
    CGSize _paletteSize;
    NSString *_paletteName;
    NSUInteger _paletteCount;
    UIScrollView *_paletteView;
    NSUInteger _brushWidth;
    CGPoint lastOffset;
    NSMutableArray *arrayOfPaths;
}

@property(unsafe_unretained)id<DrawViewDelegate> delegate;
@property(nonatomic, strong) UIColor *paintColor;
@property(nonatomic, strong) UIImage *imagePochoir;
@property(nonatomic, strong) NSDictionary *imageMotif;
@property(nonatomic, strong) UIColor *textureName;
@property(nonatomic, strong) UIButton *buttonBrush;
@property(nonatomic, strong) UIButton *buttonBrushSize;
@property(nonatomic, strong) UIButton *buttonHome;
@property(nonatomic, strong) UIButton *buttonPhoto;
@property(nonatomic, strong) UIButton *buttonAlbum;
@property(nonatomic, strong) UISlider *slider;
@property(nonatomic, readwrite) BOOL controllerVisible;

-(void)erase;
-(void)loadAnimation;
-(void)loadCanvas;
-(void)drawingStartAtPoint:(CGPoint)point withPochoirName:(NSString*)pochoirName;

@end

@protocol DrawViewDelegate <NSObject>

-(void)drawView:(DrawView*)drawView finishedTrackingPath:(CGMutablePathRef)path inRect:(CGRect)rect;

@end

static int Random(int min, int max) {
    if(min == 0 && max == 0) return 0; else return (arc4random() % ((unsigned)max - (unsigned)min + 1)) + (unsigned)min; 
}