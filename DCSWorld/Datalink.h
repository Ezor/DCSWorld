//
//  Datalink.h
//  DCSWorld
//
//  Created by Julien Rozé on 04/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import "Instrument.h"

@interface Datalink : Instrument

@property(nonatomic, strong) IBOutlet UIView* view;
@property(nonatomic, strong) IBOutlet UIImageView *light_target1;
@property(nonatomic, strong) IBOutlet UIImageView *light_target2;
@property(nonatomic, strong) IBOutlet UIImageView *light_target3;
@property(nonatomic, strong) IBOutlet UIImageView *light_target4;
@property(nonatomic, strong) IBOutlet UIImageView *light_wingman1;
@property(nonatomic, strong) IBOutlet UIImageView *light_wingman2;
@property(nonatomic, strong) IBOutlet UIImageView *light_wingman3;
@property(nonatomic, strong) IBOutlet UIImageView *light_wingman4;
@property(nonatomic, strong) IBOutlet UIImageView *light_all;
@property(nonatomic, strong) IBOutlet UIImageView *light_clear;
@property(nonatomic, strong) IBOutlet UIImageView *light_dlingress;
@property(nonatomic, strong) IBOutlet UIImageView *light_sendmem;

@end
