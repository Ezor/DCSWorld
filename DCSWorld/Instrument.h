//
//  Instrument.h
//  DCSWorld
//
//  Created by Julien Rozé on 04/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface Instrument : UIView

@property(nonatomic, strong) ViewController *mainController;

-(id)initWithMainController:(ViewController*)mainController;
-(void)updateWithData:(NSDictionary*)data;
-(CGRect)getFrame;
-(void)layout;

@end
