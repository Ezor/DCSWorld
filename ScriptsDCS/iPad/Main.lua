--Change this value to true to enable the debug log.
local enableDebugLogFile = true
--Change this to a location on your computer where you would like the debug log written to.
local debugLogFileName = "C:\\iPad.log"

-- High frequency update interval
local updateInterval = .06

-- Iterations of high frequency update to do low frequency updates
local lowFrequencyUpdateFrequency = 2

-- How often to sync, even if values haven't changed.
local periodicSyncFrequency = 30

local lastHighFrequencyMessage = ""
local lastLowFrequencyMessage = ""
local lowCSMessage = ""
local logFile = nil
local lowFrequencyUpdateCounter = 0
local periodicSyncCounter = 0
local udpSender = nil
local udpListener = nil
local lastClientIP = nil

function splitStr(str, delimiter)
	--If there's not a delimiter in the string, return the string.
	if string.find(str, delimiter) == nil then
		return { str }
	end

	local returnTable = {}
	local pattern = "(.-)" .. delimiter

	local lastPos = 0

	local startPosition, endPosition, capturedData = string.find(str, pattern, 1)

	while startPosition do
		if capturedData ~= "" then
			table.insert(returnTable, capturedData)
		end

		lastPos = endPosition + 1
		startPosition, endPosition, capturedData = string.find(str, pattern, lastPos)
	end

	-- If there's still data left in the string, return it in the last position
	if lastPos <= #str then
		table.insert(returnTable, string.sub(str, lastPos))
	end
	return returnTable
end

function WriteToLog(message)
	if logFile then
		logFile:write(string.format("%s: %s\r\n", os.date("%H:%M:%S"), message))
		logFile:flush()
	end
end

function WriteHighFrequencyInfo()
	return false
end

function WriteCSInfo()
  if udpSender then
	WriteToLog("WriteCSInfo called.")
	local panel = GetDevice(0)
	local selfData = LoGetSelfData()

	--local latitude, longitude = LoLoCoordinatesToGeoCoordinates(selfData.Position.x, selfData.position.z)
	local panelData = string.format("{\"datalink_all\" : %0.1f, \"datalink_wingman1\" : %0.1f, \"datalink_wingman2\" : %0.1f, \"datalink_wingman3\" : %0.1f, \"datalink_wingman4\" : %0.1f, \"datalink_target1\" : %0.1f, \"latitude\" : %f, \"longitude\" : %f }",
			panel:get_argument_value(16), -- DataLink All
			panel:get_argument_value(17), -- DataLink Wingman 1
			panel:get_argument_value(18), -- DataLink Wingman 2
			panel:get_argument_value(19), -- DataLink Wingman 3
			panel:get_argument_value(20), -- DataLink Wingman 4
			panel:get_argument_value(21), -- DataLink Target 1
			selfData.LatLongAlt.Lat,
			selfData.LatLongAlt.Long
			)

		WriteToLog("CSInfo "..panelData)

	if (lowCSMessage ~= panelData) or (periodicSyncCounter > periodicSyncFrequency) then
		WriteToLog("WriteCSInfo sended.")
		--socket.try()
		local result, sendError = udpSender:send(panelData)
		if sendError then
			WriteToLog(string.format("WriteCSInfo %s", sendError))
		end
		lowCSMessage = panelData
		return true
	end
  end
  return false
end

function WriteLowFrequencyInfo()
	return false
end

do
	--Hook
	local PreviousExportStart=LuaExportStart;

	LuaExportStart=function()
		package.path  = package.path..";.\\LuaSocket\\?.lua"
		package.cpath = package.cpath..";.\\LuaSocket\\?.dll"

		socket = require("socket")

		--Set up connection
		udpListener = socket.udp()
		udpListener:setsockname("0.0.0.0", 23000)
		udpListener:settimeout(.001)
		ip, port = udpListener:getsockname()

		if enableDebugLogFile then
			logFile = io.open(debugLogFileName, "w+")
		end
		
		WriteToLog("Socket created.")

		--LoSetSharedTexture("RMFD")
		--LoSetSharedTexture("LMFD")
	
		lowFrequencyUpdateCounter = 0

		if PreviousExportStart then
			PreviousExportStart();
		end
	end
end

do
	--Hook
	local PreviousExportBeforeNextFrame = LuaExportBeforeNextFrame;

	LuaExportBeforeNextFrame=function()
		local commandData, ip, port = udpListener:receivefrom()
		
		if commandData then
			WriteToLog(string.format("Received UDP data: %s %s", commandData, ip))
			local commandTable = splitStr(commandData, ",")

			for i, v in ipairs(commandTable) do WriteToLog(string.format("Captured: %s, %s", i, v)) end
			if #commandTable == 4 then
				WriteToLog(string.format("Split data into Device: %s Action: %s Value: %s", commandTable[2], commandTable[3], commandTable[4]))
				if commandTable[1] == "C" then
					--LoSetCommand
					if commandTable[2] == "-9999" then
						WriteToLog(string.format("Sending LoSetCommand(%s, %s)", commandTable[3], commandTable[4]))
						LoSetCommand(commandTable[3], commandTable[4])
					end
					local dev = GetDevice(commandTable[2])
					if dev then
						WriteToLog("performClickableAction")
						dev:performClickableAction(commandTable[3], commandTable[4])
					end
					if commandTable[2] == 53 then --Send ILS twice because something weird is happening.
						WriteToLog("performClickableAction ILS")
						dev:performClickableAction(commandTable[3], commandTable[4])
					end
				else
					WriteToLog("UDP data was parsed into a four part command, but the first argument wasn't correct.")
				end
			else
				WriteToLog("Unable to parse UDP data into a four part command.");
			end
			
			if not udpSender then
				udpSender = socket.udp()
			end
			
			if (not lastClientIP) or (lastClientIP ~= ip) then
				WriteToLog(string.format("setup udpSender %s", ip))
				udpSender:setpeername(ip, 23000)
			end
		end
		
		if PreviousExportBeforeNextFrame then
			PreviousExportBeforeNextFrame();
		end
	end
end

do
	local PreviousExportAfterNextFrame=ExportAfterNextFrame;
	LuaExportAfterNextFrame=function()

		if PreviousExportAfterNextFrame  then
			PreviousExportAfterNextFrame();
		end
	end
end

do
	local PreviousExportStop=ExportStop;
	LuaExportStop=function()

		if udpListener then
			udpListener:close()
		end

		if logFile then
			logFile:flush()
			logFile:close()
			logFile = nil
		end

		if PreviousExportStop  then
			PreviousExportStop();
		end
	end
end

do
	local PreviousExportActivityNextEvent=LuaExportActivityNextEvent;

	LuaExportActivityNextEvent=function(t)
		local tNext = t + updateInterval

		local hfSynced = false
		local lfSynced = false
		local csSynced = false

		WriteToLog("LuaExportActivityNextEvent called.")
		--if (type(mainPanel) == "table") then
			hfSynced = WriteHighFrequencyInfo()

			if (lowFrequencyUpdateCounter >= lowFrequencyUpdateFrequency) then
				lfSynced = WriteLowFrequencyInfo()
				csSynced = WriteCSInfo()

				lowFrequencyUpdateCounter = 0
			else
				lowFrequencyUpdateCounter = lowFrequencyUpdateCounter + 1
			end

			if (hfSynced == false) and (lfSynced == false) and (csSynced == false) then
				periodicSyncCounter = periodicSyncCounter + 1
			else
				periodicSyncCounter = 0
			end
		
			if PreviousExportActivityNextEvent then
				PreviousExportActivityNextEvent(t);
			end
		--end

		return tNext
	end	
end